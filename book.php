<?php
require_once("src/Disney.php");

/*
    nodeValue -> Retrieves the text string of an element
    [@(attribute)="(name)"] -> gets a specific attribute with a given name
    [@id='RobertDowneyJr']
    contains(<element>, "string")
*/

$disney = new Disney("data/Disney.xml");

$link = $disney->getActorStatistics();

$disney->removeUnreferencedActors();

$link = $disney->getActorStatistics();

?>
