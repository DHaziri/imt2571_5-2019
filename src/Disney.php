<?php
/**
  * Class for accessing DOM data representation of the contents of a Disney.xml
  * file
  */
class Disney
{
    /**
      * The object model holding the content of the XML file.
      * @var DOMDocument
      */
    protected $doc;

    /**
      * An XPath object that simplifies the use of XPath for finding nodes.
      * @var DOMXPath
      */
    protected $xpath;

    /**
      * param String $url The URL of the Disney XML file
      */
    public function __construct($url)
    {
        $this->doc = new DOMDocument();
        $this->doc->load($url);
        $this->xpath = new DOMXPath($this->doc);
    }

    /**
      * Creates an array structure listing all actors and the roles they have
      * played in various movies.
      * returns Array The function returns an array of arrays. The keys of they
      *               "outer" associative array are the names of the actors.
      *                The values are numeric arrays where each array lists
      *                key information about the roles that the actor has
      *                played. The elments of the "inner" arrays are string
      *                formatted this way:
      *               'As <role name> in <movie name> (movie year)' - such as:
      *               array(
      *               "Robert Downey Jr." => array(
      *                  "As Tony Stark in Iron Man (2008)",
      *                  "As Tony Stark in Spider-Man: Homecoming (2017)",
      *                  "As Tony Stark in Avengers: Infinity War (2018)",
      *                  "As Tony Stark in Avengers: Endgame (2019)"),
      *               "Terrence Howard" => array(
      *                  "As Rhodey in Iron Man (2008)")
      *               )
      */

    public function checkActorPlayedInMovie($actorID) : array
    {
        $mList = $this->xpath->query("//Disney/Subsidiaries/Subsidiary/Movie");
        $text = array();
        $num = 0;

        foreach ($mList as $item)
        {
          $cast = $item->getElementsByTagName("Cast")->item(0);
          $name = $item->getElementsByTagName("Name")->item(0);
          $year = $item->getElementsByTagName("Year")->item(0);

          foreach ($cast->getElementsByTagName("Role") as $role)
          {
            if ($actorID == $role->getAttribute("actor"))
            {
              /* if multiple associative arrays are needed,
                  you will have to place them inside another array:
                  $actor['Name'] = ...
                  $actor['Year'] = ...
                  $result[] = $actor
              */
              $actor = $role->getAttribute("name");
              $title = $name->nodeValue;
              $year = $year->nodeValue;

              $text[$num++] = "As " . $actor . " in " . $title
                                      . " (" . $year . ")";
              //echo $text[$num++];
            }
          }
        }

        return $text;
    }

    public function getActorStatistics() : array
    {
      $aList = $this->xpath->query("//Disney/Actors/Actor");
      $result[] = array();

      foreach ($aList as $item)
      {
                // if only one associative array is needed, this is the solution
        $id = $item->getElementsByTagName("Name")->item(0)->nodeValue;
        $actorId = $item->getAttribute("id");
                // Finds movies said actor has played in
        $result[$id] = $this->checkActorPlayedInMovie($actorId);

      }

      echo count($result) . "<br>";

      // Arraylist format: $list[(Actor Name)][(Movie Number)]
      //               ex. $result['Robert Downey Jr'][0]
      return $result;
    }

    /**
      * Removes Actor elements from the $doc object for Actors that have not
      * played in any of the movies in $doc - i.e., their id's do not appear
      * in any of the Movie/Cast/Role/@actor attributes in $doc.
      */
    public function removeUnreferencedActors()
    {
                // Retrieves list
      $list = $this->getActorStatistics();
                // Finds path for all actors in xml file
      $actors = $this->xpath->query("//Disney/Actors/Actor");

      foreach ($actors as $actor)
      {
                // stores the actor name
        $id = $actor->getElementsByTagName("Name")->item(0)->nodeValue;

                /*
                  $list array have the structure
                  $list[(actor id)][(movie number)]. So using
                  count($list[$id]) will the amount of movies, if none;
                  run code
                */
        if (count($list[$id]) == 0)
        {
                // Removes the actor and saves the document
          $removedActor = $actor->parentNode->removeChild($actor);
          $this->doc->saveXML();
          echo $removedActor->nodeValue . "<br>";
        }

      }

    }

    /**
      * Adds a new role to a movie in the $doc object.
      * @param String $subsidiaryId The id of the Disney subsidiary
      * @param String $movieName    The name of the movie of the new role
      * @param Integer $movieYear   The production year of the given movie
      * @param String $roleName     The name of the role to be added
      * @param String $roleActor    The id of the actor playing the role
      * @param String $roleAlias    The role's alias (optional)
      */
    public function addRole($subsidiaryId, $movieName, $movieYear, $roleName,
                            $roleActor, $roleAlias = null)
    {
      $cast = $this->xpath->query("//Disney/Subsidiaries/Subsidiary/Movie[Name='$movieName']/Cast");

      $newNode = $this->doc->createElement("Role");
      $newNode->setAttribute("name", $roleName);
      if ($roleAlias != null)
      {
        $newNode->setAttribute("alias", $roleAlias);
      }
      $newNode->setAttribute("actor", $roleActor);

      foreach ($cast as $roles)
      {
        $role = $roles->appendChild($newNode);
        $this->doc->saveXML();
      }

    }
}


?>
